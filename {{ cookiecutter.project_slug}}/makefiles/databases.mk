.PHONY: db
db: ## [Database] Run postgres
	@docker compose -f ${DOCKER_COMPOSE_FILE} up -d postgres

.PHONY: db-recreate
db-recreate: ## [Database] Delete and recreate postgres image
	@docker compose -f ${DOCKER_COMPOSE_FILE} stop postgres \
	&& docker compose -f ${DOCKER_COMPOSE_FILE} rm -sf postgres \
	&& docker compose -f ${DOCKER_COMPOSE_FILE} down -v \
	&& docker compose -f ${DOCKER_COMPOSE_FILE} up -d postgres
